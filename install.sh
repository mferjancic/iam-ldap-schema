echo "Setup..."
rm -rf /tmp/ldap_config
mkdir -p /tmp/ldap_config
ls -la /etc/ldap/slapd.d/cn\=config/cn\=schema
rm /etc/ldap/slapd.d/cn\=config/cn\=schema/cn={4}iam.ldif
cp iam.schema /etc/ldap/schema
echo "Slaptest - extracting ldifs"
slaptest -f ./test.conf -F /tmp/ldap_config
cp /tmp/ldap_config/cn\=config/cn\=schema/cn={4}iam.ldif /etc/ldap/slapd.d/cn\=config/cn\=schema/
chown openldap:openldap /etc/ldap/slapd.d/cn\=config/cn\=schema/cn={4}iam.ldif
echo "READY TO RESTART SLAPD"
